# Phillip Ionkov
***

![Jane Phillips/The New Mexican](https://bloximages.newyork1.vip.townnews.com/santafenewmexican.com/content/tncms/assets/v3/editorial/7/72/77263166-56fa-5737-a3e8-fd93102df78b/53fb3fcd22ec3.image.jpg?resize=1500%2C1018)
<div style="text-align: right"><i><small>Jane Phillips/The New Mexican</small></i></div>

This is **Phillip Ionkov**'s website.

* Columbia '26, [Rabi Scholar](https://urf.columbia.edu/urf/research/rabi)
* Expected BA in Physics.
* RSI 2021 Participant.
* Second Place in Physics and Astronomy Category at Regeneron ISEF 2021.
