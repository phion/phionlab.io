# Music

Student of [Janna
Warren](https://www.facebook.com/JannaWarrenPianoStudio).

***
## 2022

Jackie McGehee Young Artists' Competition - JMYAC

[J.S. Bach Concerto #1 in D Minor](https://youtu.be/t5RxvzAIBkQ)

{% youtube %}https://youtu.be/t5RxvzAIBkQ{% endyoutube %}

Graduation Recital 

[F. Chopin Ballade #1, Op 23](https://youtu.be/k43UbYjDW8U)

{% youtube %}https://youtu.be/k43UbYjDW8U{% endyoutube %}

## 2020

Honorable mention in 2020 New Mexico MTNA competition.

[Lidwig Van Beethoven Sonata in C Minor, Op. 13](https://youtu.be/TqDTagyeA8w)

{% youtube %}https://youtu.be/TqDTagyeA8w{% endyoutube %}

[S. Rachmaninoff Prelude in G Major, Op. 23, #1](https://youtu.be/DWKFJfBZwDU)

{% youtube %}https://youtu.be/DWKFJfBZwDU{% endyoutube %}

[G. Gershwin 3 Preludes 1. Allegro ben Ritmato e deciso](https://youtu.be/cJAEH92mMEg)

{% youtube %}https://youtu.be/cJAEH92mMEg{% endyoutube %}

[G. Gershwin 3 Preludes 2. Andante con moto e poco rubato](https://youtu.be/Wvo6xqi-EB8)

{% youtube %}https://youtu.be/Wvo6xqi-EB8{% endyoutube %}

## 2019

[S. Rachmaninoff Prelude in G Minor](https://youtu.be/2p7lrmmFaBM)

{% youtube %}https://youtu.be/2p7lrmmFaBM{% endyoutube %}

[Christmas Recital](https://youtu.be/G14yUxYDytg)

{% youtube %}https://youtu.be/G14yUxYDytg{% endyoutube %}

## 2018

Second Place in the 2018 New Mexico MTNA competition.

[V.A. Gavrilin Toccata in C Major](https://www.youtube.com/watch?v=j92OsLs3jgk)

{% youtube %}https://www.youtube.com/watch?v=j92OsLs3jgk{% endyoutube %}

[J.S. Bach Prelude and Fugue in D Minor BWV 851](https://youtu.be/g5lvSDCDTLo)

{% youtube %} https://youtu.be/g5lvSDCDTLo{% endyoutube %}

## 2017

Second Place in the 2017 PMTNM Honors Northeast District Competition.

[F. Schubert Impromptu Op. 90, D. 899, #2](https://youtu.be/x1U2Nul0X3Q)

{% youtube %}https://youtu.be/x1U2Nul0X3Q{% endyoutube %}

## 2014

Second Place in the 2014 PMTNM Honors State Competition.

{% youtube %}https://youtu.be/vP1F5G_rz6U{% endyoutube %}

{% youtube %}https://youtu.be/10rsZm3rxWM{% endyoutube %}

