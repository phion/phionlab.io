# [FIRST Robotics Competition](https://www.firstinspires.org/robotics/frc)

Member of [Team 4153 - Project Y](http://team4153.org).

![](frc/IMG_4225.jpeg)
## Season 2021

* Lead of the PEP (programming, electronics, and pneumatics) team. 
* Drive Team Technician

### FIRST Championship - Houston

* **Fourth Place in Einstein Division**
* **Championship Division Winner (Carver)**
* Alliance 1 Second Pick

![](frc/IMG_6295.jpeg)

### FIRST In Texas District Championship Houston

* District Engineering Inspiration Award
* Alliance 7 Second Pick

### FIT District Amarillo Event

* Engineering Inspiration Award
* Alliance 4 Captain

![](frc/IMG_6214.jpeg)

### FIT District Fort Worth Event

* Gracious Professionalism Award
* Alliance 5 Captain

![](frc/IMG_6148.jpeg)

## Season 2020

* Co-lead of the mechanical team.
* Season cancelled due to COVID-19.

## Season 2019

https://frc-events.firstinspires.org/team/4153

### FIT District El Paso Event

* District Event Finalist
* Industrial Design Award

### FIT District Amarillo Event

* Innovation in Control Award

### FIRST In Texas District Championship

* Team Spirit Award

### FIRST Championship - Houston

