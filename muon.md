#  Measuring and Modeling the Energy Spectrum of Cosmic Ray Muons

with [Anthony Lestone](https://anthonylestone.github.io).

***

## Awards

* **Regeneron International Science and Engineering Fair 2020 Finalist**

* ISEF 2020 ** Cancelled **
* New Mexico State Science Fair ** Cancelled **
* Northeastern NM Regional Science and Engineering Fair:
	- Best in Show Runner Up (ISEF 2020 qualification).
	- 1st Place in Senior Physics Category.

{% pdf src="muon/Measuring_and_Modeling_the_Energy_Spectrum_of_Cosmic_Ray_Muons.pdf", width="100%", height="700", type="application/pdf" %}{% endpdf %}

