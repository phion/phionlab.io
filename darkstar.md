# Dark Star
***

## Awards

* Northeastern NM Regional Science and Engineering Fair: 1st Place in
Junior Physics Category.

* Northeastern New Mexico Junior Academy of Science Scientific
Research Paper Junior Competition: 2nd Place.

* New Mexico Science and Engineering Fair: 1st Place in Junior Physics
Category.

<a href="darkstar/dark-star-landscape.pdf" target="_blank">Poster (PDF)</a>

## Research Question

How will interstellar bodies of different masses entering the Solar
System affect the probability for life on Earth to go extinct?

## Hyphothesis

If an interstellar object enters the Solar System, then the
Earth's orbit will be affected the most by the bodies of at
least one solar mass because those are as great as that of the sun, so
they will affect the Earth the most.

## Introduction

### Theory

$$\begin{eqnarray*}
F_i &=& G \frac{Mm_i}{|r_i|^3}r_i \\
\\
F &=& \sum_{i=0}^n F_i = GM\sum_{i=0}^n\frac{m_ir_i}{|r_i|^3}\\
\\
F &=& Ma \\
\\
a &=& G \sum_{i=0}^n \frac{m_ir_i}{|r_i|^3}\\
\\
\Delta V &=& a \Delta t \\
\\
\Delta P &=& V \Delta t + \frac{a\Delta t^2}{2}
\end{eqnarray*}$$

### Types of Stars

![Hertzsprung-Russell (H-R) diagram](darkstar/Hertzsprung-Russel_StarData.png)

*Figure 1: Hertzsprung-Russell (H-R) diagram[1]*

The Hertzsprung-Russell (H-R) diagram is a star chart that classifies
stars by temperature and luminosity.

It defines different types of stars:
   * Main sequence stars
   * Giants
   * White dwarfs
   * Brown dwarfs
   
The stars that are used in this project are:
   * Brown dwarf - 0.01 to 0.05 Solar masses ($$M_\odot$$)
   * Black dwarf - 1 Solar mass
   * Stellar black hole - 5 Solar masses

## Solar System

![The Solar System](darkstar/solarsystem.jpg)
*Figure 2: The Solar System[2]*

### Solar System Regions

* Inner planets
   * Rocky, warm, small planets
   * "Habitable Zone" - zone in which life can survive (0.8 AU - 2
   AU from the Sun).
   
* Asteroid belt: area of asteroids that separates the inner from
the outer planets

* Outer planets: gaseous, cold, massive planets

* Kuiper belt: belt of asteroids, comets, and dwarf planets past
the orbit of Neptune

* Heliosheath: region of space in which solar wind is stopped by
interstellar wind

* Oort cloud: hypothetical region of space thought to contain the
long term comets that orbit the Sun


### Velocities

| Body  | AU/day |
| ----- | ------ |
| Mercury | 0.028 |
| Venus | 0.02 |
| Earth | 0.017 |
| Mars | 0.014 |
| Jupiter | 0.0075 |
| Saturn | 0.0056 |
| Uranus | 0.0039 |
| Neptune | 0.0031 |
| *Oumuamua* | 0.015 |
| *Luhman 16* | 0.012 |
| *Proxima Centauri* | 0.013 |
| Barnard's Star | 0.063 |


## Interstellar Neighborhood

### Single Stars

* Barnard's Star - 5.9 light years ($$0.144 M_\odot$$)
* WISE 0855-0714 - 7.3 light years ($$0.003 - 0.01 M_\odot$$)
* Wolf 359 - 7.8 light years ($$0.09 M_\odot$$)
* Lalande 21185 - 8.3 light years ($$0.46 M_\odot$$)

### Binary Systems

* Alpha Centauri - 4.3 light years
  * Alpha Centauri A ($$1.1 M_\odot$$)
  * Alpha Centauri B ($$0.907 M_\odot$$)
  
* Luhman 16 - 6.5 light years
  * Luhman A ($$0.033 M_\odot$$)
  * Luhman B ($$0.027 M_\odot$$)
  
* Sirius - 8.6 light years
  * Sirius A ($$2.063 M_\odot$$)
  * Sirius B ($$1.018  M_\odot$$)

** _Earth's life can be destroyed in several ways. One way is if asteroids
or comets collide with Earth. This can be caused by the interstellar
object disrupting the Oort cloud or the Kuiper or asteroid belt._ **

** _Another reason for life's extinction can be if the gravitational force
of the interstellar body (or one of the planets) knocks off the Earth
out of the Habitable Zone. This event will be the focus for my
project._ **

### N-body Simulator: Rebound

https://rebound.readthedocs.io/en/latest/

* N-body simulator
* Python and C extensible
* Good documentation and examples
* Can show the planets and stars trajectories while the simulation is
running

### Approach

1. Create an N-body gravitational simulator.
2. Add the bodies of the solar system to the simulation.
3. Add the different dark stars to the program.
4. For each simulation, change the position and the trajectory of the
dark star.
5. After using all the velocities assigned to one dark star, change the
mass.
6. Repeat Steps 4 and 5 for the all three types of dark bodies.
7. Find probability of Earth leaving the Habitable Zone, maximum distance
that the star can get to the Sun while affecting Earth, and the
minimum distance that the star can get to the Sun that does not affect
Earth.

### Testing Parameters

* Simulated time: 200 years
* Dark star masses (in $$M_\odot$$):
  * Brown dwarf - 0.05 
  * Black dwarf - 1
  * Stellar black hole - 5

* Spherical coordinate system: 
  * Uses $$\varphi$$, $$\theta$$ instead of $$x$$, $$y$$, and $$z$$.
  * Varying $$\varphi$$ and $$\theta$$ form a sphere around the center.

    $$\begin{eqnarray*}
    x &=& r \sin\theta \cos\varphi \\
    y &=& r \sin\theta \sin\varphi \\
    z &=& r \cos\varphi \\
    \end{eqnarray*}$$

* Starting position:
  * Distance = 50 AU
  * $$\varphi$$ and $$\theta$$ change by 8 degrees.

* Starting trajectory:
  * Velocities (AU/day): 0.001, 0.003, 0.005, 0.01, 0.015, 0.02, 0.025, 0.06
  * Direction: $$\varphi_v$$ and $$\theta_v$$ change by 7 degrees.

* Simulations:
  * Approximately 700,000 simulations for each velocity on each mass.
  * Total of 16,800,000 simulations.

## Results

### Probability of Earth's Life Extinction

![YYY](darkstar/plot.png)
Figure 3: Effect of velocity on probability of Earth's life extinction
for simulated dark stars

![YYY](darkstar/prob2.png)
Figure 4: Effect of velocity on probability of Earth's life extinction
for three neighboring binary systems

### Other Results

![YYY](darkstar/max.png)
Figure 5: Maximum distance from the Sun that still affects the Earth
for simulated dark stars

![YYY](darkstar/bmax.png)
Figure 6: Maximum distance from the Sun that still affects the Earth
for binary systems

![YYY](darkstar/min.png)
Figure 7: Minimum distance from the Sun that does not affect Earth for
simulated dark star

![YYY](darkstar/bmin.png)
Figure 8: Minimum distance from the Sun that does not affect Earth for
binary systems


### Brown Dwarf: Effects on Earth's Orbit

![YYY](darkstar/m005d1.png)
![YYY](darkstar/m005d2.png)
![YYY](darkstar/m005d3.png)
![YYY](darkstar/m005d4.png)

### Black Dwarf: Effects on Earth's Orbit

![YYY](darkstar/m1d1.png)
![YYY](darkstar/m1d2.png)
![YYY](darkstar/m1d3.png)
![YYY](darkstar/m1d4.png)

### Black Hole: Effects on Earth's Orbit

![YYY](darkstar/m5d1.png)
![YYY](darkstar/m5d2.png)
![YYY](darkstar/m5d3.png)
![YYY](darkstar/m5d4.png)

## Conclusions

* Probability for life extinction depends on the mass and velocity
of the incoming celestial body.
* Brown star can get as close as Jupiter's orbit before knocking
the Earth out of the habitable zone.
* Stellar black hole can get closer than Earth's orbit to the Sun
without knocking the Earth out of the habitable zone.
* Future steps include finding the average amount of time the
Earth has before all life is killed.

## References

1. European Southern Observatory. Hertzsprung-Russell Diagram.
https://www.eso.org/public/images/eso0728c/
2. European Space Agency. Location of comets in the Solar System.
http://sci.esa.int/jump.cfm?oid=49390
3.  Rein, Hanno, and S-F. Liu. "REBOUND: an open-source
multi-purpose N-body code for collisional dynamics." Astronomy &
Astrophysics 537 (2012): A128.
4. Wikipedia. February 1 2018. Dark star (Newtonian mechanics).
https://en.wikipedia.org/wiki/Dark_star_(Newtonian_mechanics%29
5. Dehnen, Walter, and Justin I. Read. "N-body simulations of
gravitational dynamics." The European Physical Journal Plus 126.5
(2011): 1-28.
6. Wikipedia. February 17 2018. Brown dwarf.
https://en.wikipedia.org/wiki/Brown_dwarf
7. "JPL Solar System Dynamics." NASA, NASA, http://ssd.jpl.nasa.gov/.
8. Wikipedia. January 4 2018. Luhman 16.
https://en.wikipedia.org/wiki/Luhman_16
9. Wikipedia. February 10 2018. Solar System.
https://en.wikipedia.org/wiki/Solar_System
10. NASA. JPL Horizons System. JPL.
https://ssd.jpl.nasa.gov/horizons.cgi
11. Burgasser, Adam J. "Brown dwarfs: Failed stars, super Jupiters."
Physics Today 61.6 (2008): 70.
12. Mamajek, Eric E., et al. "The Closest Known Flyby of a Star to
the Solar System." The Astrophysical Journal Letters 800.1 (2015): L17.
13. Binney, James, and Scott Tremaine. Galactic dynamics. Princeton
university press, 2011.
