# Science Fair Projects
***

* [2021: Nondestructive Analysis of Geological Sites Through Muon
Transmission Imaging](mugraph.md)
* [2020: Measuring and Modeling the Energy Spectrum of Cosmic Ray Muons](muon.md)
* [2019: Experimental Validation of Quantum Entanglement Using
Affordable Apparatus](entanglement.md)
* [2018: Dark Star](darkstar.md)
