# Summary

* [Resume](Resume.md)
* [Introduction](README.md)
* [Science Fair](ScienceFair.md)
	* [Nondestructive Archeology using Muography (2021)](mugraph.md) 
	* [Cosmic Ray Muons Study (2020)](muon.md) 
	* [Validating Quantum Entanglement (2019)](entanglement.md)
	* [Dark Star (2018)](darkstar.md)
* [FIRST Robotics](FRC.md)
* [New Mexico Supercomputing Challenge](Supercomputing.md)
* [Music](Music.md)
* [Dance](Dance.md)

