# Supercomputing Challenge
***

[New Mexico Supercomputing Challenge](https://supercomputingchallenge.org)

"The Supercomputing Challenge is a program for elementary, middle, and
high schools students to come up with a computational science project
and work on it all year long."

## 2017-2018

Project Name: Interpreting and Classifying Music

Team Members:
* Andy Corliss
* Max Corliss
* Phillip Ionkov
* Ming Lo

[Final Report](https://www.supercomputingchallenge.org/17-18/myTeam/teamData/LAMS155/InterpretingandClassifyingMusicFinalReport.pdf)


## 2016-2017

Project Name: Solving Sudoku

Team Members:
* Andy Corliss
* Max Corliss
* Phillip Ionkov
* Ming Lo

[Final Report](https://supercomputingchallenge.org/16-17/fr_submitted/lams4-report.pdf)

## 2015-2016

Project Name: Solving the Rubik's Cube 2.0
Team Members:
* Andy Corliss
* Max Corliss
* Phillip Ionkov
* Ming Lo

### Awards:

* First Place

[Final Report](supercomputing/final.pdf)

![](https://supercomputingchallenge.org/16-17/sites/all/themes/responsive_blog/images/slide-image-2.jpg)

## 2014-2015

Project Name: Solving the Rubik's Cube
Team Members:
* Andy Corliss
* Max Corliss
* Phillip Ionkov
* Ming Lo

### Awards:

* Science Rocks

[Final Report](https://www.supercomputingchallenge.org/14-15/finalreports/10.pdf)
