#  Nondestructive Analysis of Geological Sites Through Muon Transmission Imaging

with [Anthony Lestone](https://anthonylestone.github.io).

***

## Awards

* **Second Place in Physics and Astronomy**: Regeneron ISEF 2021.


* New Mexico State Science Fair
	- Second Place in Physical Sciences category and ISEF Finalist.
	- First Place in Physics and Astronomy category.


* Northeastern NM Regional Science and Engineering Fair:
	- 1st Place in Senior Physics category.

{% pdf src="muography/SciFairISEF.pdf", width="100%", height="700", type="application/pdf" %}{% endpdf %}

