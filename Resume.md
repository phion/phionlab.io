Phillip Ionkov
============
[pli2102@columbia.edu](mailto:pli2102@columbia.edu)  

Education
---------
**Columbia University**, New York, NY  
*Columbia College* B.A., Physics  
Cumulative GPA: 3.77, expected graduation May 2026  
  
**Los Alamos High School**, Los Alamos, NM  
Cumulative GPA: 4.33, graduated May 2022

Research Experience
----------

> June 2022 - August 2022  
> 

**Los Alamos National Laboratory**, Los Alamos, NM  
*Undergraduate Research Intern - RAMPART developer*  

Implemented optical physics in RAMPART, a Geant4 plugin that uses macros to simplify user experience. Acquired experience in UNIX, C/C++, Geant4, ROOT, and simulated optical physics.


> June 2021 - August 2021  
> 

**Research Summer Institute (RSI)**, Virtual  
*Summer research program for top 80 high school students in the world*  

Designed a system to store and construct telescope mirrors in order to improve efficiency in space telescope launches. The design included a novel approach of sectioning mirrors for compact storage within the satellite during transport.
Acquired experience in mechanical design, CAD (specifically Onshape and Fusion360), and in space telescope function.


> November 2020 - May 2021  
> 

**Nondestructive Analysis of Geological Sites Through Muon Transmission Imaging**  

*2nd place in Physics & Astronomy, ISEF 2021*  

Observed anomalies in muon flux to identify caverns or high-density pockets in geographical structures. Designed a simple experimental setup that can easily be taken to any object of interest to search for anomalies. Wrote a C simulation to replicate the experiment using ray-casting algorithms. \\
Acquired experience in experimental design, detector design, and C


> September 2019 - May 2020  
> 

**Measuring and Modeling the Energy Spectrum of Cosmic Ray Muons**  
 
*Finalist, ISEF (International Science and Engineering Fair) 2020*  
 
Measured the energy of cosmic muons with homemade detectors. Detectors could not directly measure particle energy, so the experiment was set up to use blocks of lead to create an association between particle count and particle energy. Simulated the experiment in Geant4 to compare with experimental data. \\
Acquired experience in particle physics research, electronics, C/C++, and Geant4


Skills
--------------------
C/C++, Java, Unix, CAD/CAM (Onshape, Solidworks, Fusion360), Geant4, ROOT
